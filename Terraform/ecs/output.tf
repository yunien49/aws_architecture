output "vpc_id" {
  description = "id of the VPC"
  value = "${aws_vpc.vpc.id}"
}
output "all_AZ" {
  value = "${data.aws_availability_zones.available.names}"
}
output "subnet_publicA_AZ" {
  value = "${aws_subnet.subnet_publicA.availability_zone}"
}
output "subnet_publicB_AZ" {
  value = "${aws_subnet.subnet_publicB.availability_zone}"
}
output "subnet_privateA_AZ" {
  value = "${aws_subnet.subnet_privateA.availability_zone}"
}
output "subnet_privateB_AZ" {
  value = "${aws_subnet.subnet_privateB.availability_zone}"
}
output "privateA_nat_eip" {
  value = "${aws_eip.nat_gatewayA_eip.public_ip}"
}
output "privateB_nat_eip" {
  value = "${aws_eip.nat_gatewayB_eip.public_ip}"
}
output "alb_dns_name" {
  value = "${aws_alb.ecs_alb.dns_name}"
}
