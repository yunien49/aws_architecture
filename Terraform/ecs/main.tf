provider "aws"{
  region = "${var.aws_region}"
}
data "aws_availability_zones" "available" {
  state = "available"
}
resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true

  tags {
    Name = "${var.environment}-vpc"
  }
}
resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name = "${var.environment}-igw"
  }
}
resource "aws_route_table" "publicA_rt" {
  vpc_id = "${aws_vpc.vpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }
}
resource "aws_route_table_association" "publicA_rta" {
  subnet_id = "${aws_subnet.subnet_publicA.id}"
  route_table_id = "${aws_route_table.publicA_rt.id}"
}
resource "aws_subnet" "subnet_publicA" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone = "${data.aws_availability_zones.available.names[0]}"

  tags {
    Name = "${var.environment}-subnet-publicA"
  }
}
resource "aws_route_table" "publicB_rt" {
  vpc_id = "${aws_vpc.vpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }
}
resource "aws_route_table_association" "publicB_rta" {
  subnet_id = "${aws_subnet.subnet_publicB.id}"
  route_table_id = "${aws_route_table.publicB_rt.id}"
}
resource "aws_subnet" "subnet_publicB" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "10.0.2.0/24"
  map_public_ip_on_launch = true
  availability_zone = "${data.aws_availability_zones.available.names[1]}"

  tags {
    Name = "${var.environment}-subnet-publicB"
  }
}
resource "aws_eip" "nat_gatewayA_eip" {
  vpc = true
}
resource "aws_nat_gateway" "nat_gatewayA" {
  allocation_id = "${aws_eip.nat_gatewayA_eip.id}"
  subnet_id = "${aws_subnet.subnet_publicA.id}"
}
resource "aws_route_table" "privateA_rt" {
  vpc_id = "${aws_vpc.vpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_nat_gateway.nat_gatewayA.id}"
  }
}
resource "aws_route_table_association" "privateA_rta" {
  subnet_id = "${aws_subnet.subnet_privateA.id}"
  route_table_id = "${aws_route_table.privateA_rt.id}"
}
resource "aws_subnet" "subnet_privateA" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "10.0.3.0/24"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"

  tags {
    Name = "${var.environment}-subnet-privateA"
  }
}

resource "aws_eip" "nat_gatewayB_eip" {
  vpc = true
}
resource "aws_nat_gateway" "nat_gatewayB" {
  allocation_id = "${aws_eip.nat_gatewayB_eip.id}"
  subnet_id = "${aws_subnet.subnet_publicB.id}"
}
resource "aws_route_table" "privateB_rt" {
  vpc_id = "${aws_vpc.vpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_nat_gateway.nat_gatewayB.id}"
  }
}
resource "aws_route_table_association" "privateB_rta" {
  subnet_id = "${aws_subnet.subnet_privateB.id}"
  route_table_id = "${aws_route_table.privateB_rt.id}"
}
resource "aws_subnet" "subnet_privateB" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "10.0.4.0/24"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"

  tags {
    Name = "${var.environment}-subnet-privateB"
  }
}