resource "aws_ecs_cluster" "ecs_cluster" {
  name = "${var.environment}-ecs-cluster"
}

data "template_file" "web_task" {
  template = "${file("task-definitions/service.json")}"
  vars {
    REPOSITORY_URL = "544459298722.dkr.ecr.us-east-1.amazonaws.com/demo"
    AWS_REGION = "${var.aws_region}"
    LOGS_GROUP = "${aws_cloudwatch_log_group.myapp.name}"
  }
}

resource "aws_security_group" "ecs_tasks_sg" {
  name        = "${var.environment}-ecs-tasks-sg"
  description = "Allow traffic for ecs"
  vpc_id      = "${aws_vpc.vpc.id}"

  ingress{
    protocol    = "tcp"
    from_port   = 8086
    to_port     = 8086
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.environment}-ecs-tasks-sg"
  }
}

resource "aws_ecs_task_definition" "task_def" {
  family = "${var.environment}-task-def"
  requires_compatibilities = ["FARGATE"]
  network_mode = "awsvpc"
  execution_role_arn = "${aws_iam_role.ecs_task_assume.arn}"
  task_role_arn = "${aws_iam_role.ecs_task_assume.arn}"
  memory = 512
  cpu = 256
  # container_definitions = "${file("task-definitions/service.json")}"
  container_definitions = "${data.template_file.web_task.rendered}"

  depends_on = ["aws_iam_role.ecs_task_assume"]
}

resource "aws_ecs_service" "ecs_service" {
  name            = "${var.environment}-ecs-service"
  cluster         = "${aws_ecs_cluster.ecs_cluster.id}"
  task_definition = "${aws_ecs_task_definition.task_def.arn}"
  desired_count   = 2
  # iam_role = "${aws_iam_role.ecs_service_role.arn}"
  launch_type     = "FARGATE"

  network_configuration {
    subnets         = ["${aws_subnet.subnet_publicA.id}", "${aws_subnet.subnet_publicB.id}"]
    security_groups = ["${aws_security_group.ecs_tasks_sg.id}"]
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = "${aws_alb_target_group.ecs_alb_tg.arn}"
    container_name   = "web"
    container_port   = 8086
  }

  depends_on = ["aws_alb_target_group.ecs_alb_tg", "aws_alb.ecs_alb"]
}


