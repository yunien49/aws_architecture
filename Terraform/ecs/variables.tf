variable "aws_region" {
  description = "aws region"
  default = "us-east-1"
}
variable "environment" {
  description = "environment"
  default = "dev"
}

variable "fargate_cpu" {
  description = "ecs cpu"
  default = 256
}
variable "fargate_memory" {
  description = "ecs memory"
  default = 512
}
variable "image_url" {
  description = "image Repository URL"
  default = "544459298722.dkr.ecr.us-east-1.amazonaws.com/demo:latest"
}

