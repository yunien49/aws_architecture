resource "aws_security_group" "ecs_alb_sg" {
  name = "${var.environment}-ecs-alb-sg"
  description= "sg of ecs alb"
  vpc_id = "${aws_vpc.vpc.id}"

  ingress{
    protocol    = "tcp"
    from_port   = 8086
    to_port     = 8086
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.environment}-ecs-alb-sg"
  }
}
resource "aws_alb" "ecs_alb" {
  name = "${var.environment}-ecs-alb"
  subnets = ["${aws_subnet.subnet_publicA.id}", "${aws_subnet.subnet_publicB.id}"]
  security_groups = ["${aws_security_group.ecs_alb_sg.id}"]

  tags {
    Name = "${var.environment}-ecs-alb"
  }
}
resource "aws_alb_target_group" "ecs_alb_tg" {
  name        = "${var.environment}-ecs-alb-tg"
  port        = 8086
  protocol    = "HTTP"
  vpc_id      = "${aws_vpc.vpc.id}"
  target_type = "ip"

  depends_on = ["aws_security_group.ecs_alb_sg"]

  lifecycle {
    create_before_destroy = true
  }
  health_check {    
    healthy_threshold   = 3    
    unhealthy_threshold = 10    
    timeout             = 5    
    interval            = 10    
    path                = "/"    
    port                = "traffic-port"
  }
}
resource "aws_alb_listener" "ecs_alb_listener" {
  load_balancer_arn = "${aws_alb.ecs_alb.id}"
  port              = 8086
  protocol          = "HTTP"
  depends_on = ["aws_security_group.ecs_alb_sg"]

  default_action {
    target_group_arn = "${aws_alb_target_group.ecs_alb_tg.id}"
    type             = "forward"
  }
}
