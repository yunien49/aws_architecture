## using cli commandline deploy stack
[CLI寫法](https://docs.aws.amazon.com/cli/latest/reference/cloudformation/index.html#cli-aws-cloudformation)
[set-stack-policy](https://docs.aws.amazon.com/cli/latest/reference/cloudformation/set-stack-policy.html)

```shell=
# create/update stack
aws cloudformation deploy --stack-name ${name} --template-file templates/xxx.yaml --parameter-overrides StackName=xxx

# 增加policy, 無法刪除，只能透過修正policy達成更新的目的
aws cloudformation set-stack-policy --stack-name ${name} --stack-policy-body file://policy/xxx.json

# protection stack
aws cloudformation update-termination-protection --stack-name ${name} --enable-termination-protection

# disable protection stack
aws cloudformation update-termination-protection --stack-name ${name} --no-enable-termination-protection

# delete stack
aws cloudformation delete-stack --stack-name ${name}

```
