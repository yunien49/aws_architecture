resource "aws_appautoscaling_target" "target" {
  min_capacity = 2
  max_capacity = 4
  resource_id = "service/${aws_ecs_cluster.ecs_cluster.name}/${aws_ecs_service.ecs_service.name}"
  role_arn = "${aws_iam_role.ecs_autoscale_role.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace = "ecs"
}

resource "aws_appautoscaling_policy" "up" {
  name                    = "${var.environment}-cpu_GreaterThan50_and_GreaterThan80"
  policy_type = "StepScaling"
  service_namespace       = "ecs"
  resource_id             = "service/${aws_ecs_cluster.ecs_cluster.name}/${aws_ecs_service.ecs_service.name}"
  scalable_dimension      = "ecs:service:DesiredCount"

  /*
    aws_cloudwatch_metric_alarm.cpu_GreaterThan50_and_GreaterThan80.threshold:50
    50+0 <= 50+30
    50+30 <= 無限
  */
  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      metric_interval_upper_bound = 30
      scaling_adjustment = 1
    }

    step_adjustment {
      metric_interval_lower_bound = 30
      scaling_adjustment = 1
    }

  }

  depends_on = ["aws_appautoscaling_target.target"]
}

/*
comparison_operator: GreaterThanOrEqualToThreshold，GreaterThanThreshold，LessThanThreshold，LessThanOrEqualToThreshold
*/
resource "aws_cloudwatch_metric_alarm" "cpu_GreaterThan50_and_GreaterThan80" {
  alarm_name          = "${var.environment}-cpu_GreaterThan50_and_GreaterThan80"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = 60
  statistic           = "Average"
  threshold           = 50

  dimensions {
    ClusterName = "${aws_ecs_cluster.ecs_cluster.name}"
    ServiceName = "${aws_ecs_service.ecs_service.name}"
  }

  alarm_actions = ["${aws_appautoscaling_policy.up.arn}"]
}

resource "aws_appautoscaling_policy" "down" {
  name                    = "${var.environment}-cpu_LessThan80_and_LessThan50"
  policy_type = "StepScaling"
  service_namespace       = "ecs"
  resource_id             = "service/${aws_ecs_cluster.ecs_cluster.name}/${aws_ecs_service.ecs_service.name}"
  scalable_dimension      = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    /*
      aws_cloudwatch_metric_alarm.cpu_LessThan80_and_LessThan50.threshold:80
      80+(0) >= 80+(-30)
      80+(-30) >= 無限
    */
    step_adjustment {
      metric_interval_upper_bound = 0
      metric_interval_lower_bound = -30
      scaling_adjustment = -1
    }

    step_adjustment {
      metric_interval_upper_bound = -30
      scaling_adjustment = -1
    }

  }

  depends_on = ["aws_appautoscaling_target.target"]
}

resource "aws_cloudwatch_metric_alarm" "cpu_LessThan80_and_LessThan50" {
  alarm_name          = "${var.environment}-cpu_LessThan80_and_LessThan50"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = 60
  statistic           = "Average"
  threshold           = 80

  dimensions {
    ClusterName = "${aws_ecs_cluster.ecs_cluster.name}"
    ServiceName = "${aws_ecs_service.ecs_service.name}"
  }

  alarm_actions = ["${aws_appautoscaling_policy.down.arn}"]
}