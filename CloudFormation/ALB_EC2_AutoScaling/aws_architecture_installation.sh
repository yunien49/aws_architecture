#!bin/bash

# Check root
# if [ "$EUID" -ne 0 ];
# then
#   echo "Please login as the root user."
#   exit
# fi

# Check aws install
aws --v
if [ $? -ne 0 ]; then
  echo "Please install aws cli."
  exit
fi

echo "Installing package..."
apt-get update
apt-get install zip
apt-get install jq
echo "Install package ok"

# check vpc-2azs.yaml
echo "Check vpc-2azs.yaml..."
cf_file="./vpc-2azs.yaml"
if [ -f "$cf_file" ]
then
  echo "ok"
else
  echo "ERROR: vpc-2azs.yaml not found."
  exit
fi

# parameters
REGION=""
ACCESS_KEY_ID=""
SECRET_ACCESS_KEY=""
STAGE_NAME=""
ClassAB=""
CENTOS69_IMAGE_ID=""
INSTANCE_TYPE=""


echo "1. us-east-1: N. Virginia | 2. us-east-2: Ohio | 3. us-west-1: N. California | 4. us-west-2: Oregon"
echo "5. ap-northeast-2: seoul  | 6. ap-southeast-1: singapore | 7. ap-northeast-1: tokyo"
options=("us-east-1" "us-east-2" "us-west-1" "us-west-2" "ap-northeast-2" "ap-southeast-1" "ap-northeast-1")
# ultraserve-centos-6.9-ami-nat-hvm-2018.03.6-13-x86_64-gp2
while [[ $REGION == '' ]]
do
  echo "1. AWS default Region: "
  select opt in "${options[@]}"
  do
    case $opt in
      "us-east-1")
        REGION=$opt
        CENTOS69_IMAGE_ID="ami-00094b08c165ad78a"
        break
        ;;
      "us-east-2")
        REGION=$opt
        CENTOS69_IMAGE_ID="ami-0f95aa0adcdc141bc"
        break
        ;;
      "us-west-1")
        REGION=$opt
        CENTOS69_IMAGE_ID="ami-0d27a10686b26ace2"
        break
        ;;
      "us-west-2")
        REGION=$opt
        CENTOS69_IMAGE_ID="ami-02b724300c1420299"
        break
        ;;
      "ap-northeast-2")
        REGION=$opt
        CENTOS69_IMAGE_ID="ami-0eef87d92925018a1"
        break
        ;;
      "ap-southeast-1")
        REGION=$opt
        CENTOS69_IMAGE_ID="ami-0609e0128152b68c5"
        break
        ;;
      "ap-northeast-1")
        REGION=$opt
        CENTOS69_IMAGE_ID="ami-0d67db47940b5df30"
        break
        ;;
      *) echo "invalid option. Please try again.";;
    esac
  done
done

while [[ $ACCESS_KEY_ID == '' ]]
do
  read -p "2. AWS access key ID: " ACCESS_KEY_ID
done 

while [[ $SECRET_ACCESS_KEY == '' ]]
do
  read -p "3. AWS secret access key: " SECRET_ACCESS_KEY
done

options=("dev" "qt" "prd")
# ultraserve-centos-6.9-ami-nat-hvm-2018.03.6-13-x86_64-gp2
while [[ $STAGE_NAME == '' ]]
do
  echo "4. running Env: "
  select opt in "${options[@]}"
  do
    case $opt in
      "dev")
        STAGE_NAME=$opt
        ClassAB="10.10"
        break
        ;;
      "qt")
        STAGE_NAME=$opt
        ClassAB="20.20"
        break
        ;;
      "prd")
        STAGE_NAME=$opt
        ClassAB="30.30"
        break
        ;;
      *) echo "invalid option. Please try again.";;
    esac
  done
done

echo "t1.micro, t2.nano, t2.micro, t2.small, t2.medium, t2.large, m1.small, 
      m1.medium, m1.large, m1.xlarge, m2.xlarge, m2.2xlarge, m2.4xlarge, m3.medium,
      m3.large, m3.xlarge, m3.2xlarge, m4.large, m4.xlarge, m4.2xlarge, m4.4xlarge,
      m4.10xlarge, c1.medium, c1.xlarge, c3.large, c3.xlarge, c3.2xlarge, c3.4xlarge,
      c3.8xlarge, c4.large, c4.xlarge, c4.2xlarge, c4.4xlarge, c4.8xlarge, g2.2xlarge,
      g2.8xlarge, r3.large, r3.xlarge, r3.2xlarge, r3.4xlarge, r3.8xlarge, r4.xlarge,
      i2.xlarge, i2.2xlarge, i2.4xlarge, i2.8xlarge, d2.xlarge, d2.2xlarge, d2.4xlarge,
      d2.8xlarge, hi1.4xlarge, hs1.8xlarge, cr1.8xlarge, cc2.8xlarge, cg1.4xlarge"
while [[ $INSTANCE_TYPE == '' ]]
do
  read -p "5. instance type : " INSTANCE_TYPE
done


echo ""
echo "Please confirm default variables below:"
echo "===================================================="
echo "  1. AWS default Region: ${REGION}"
echo "  2. AWS access key ID: ${ACCESS_KEY_ID}"
echo "  3. AWS secret access key: ${SECRET_ACCESS_KEY}"
echo "  4. running Env: ${STAGE_NAME}"
echo "  5. All EC2 instance type : ${INSTANCE_TYPE}"
echo "===================================================="
echo ""

confirm=""
while [[ ! "$confirm" =~ ^[Y|y|N|n] ]]
do
  read -p "Do you want to continue? [y/n]" confirm
  if [[ "$confirm" =~ ^[N|n] ]]; then
    echo "Exit installation."
    exit
  fi
done

# set aws configure
echo "Set AWS Configure..."
# # for windows
# aws configure set aws_access_key_id $ACCESS_KEY_ID 
# aws configure set aws_secret_access_key $SECRET_ACCESS_KEY
# aws configure set default.region $REGION
# for Mac, Linux
export AWS_ACCESS_KEY_ID=$ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$SECRET_ACCESS_KEY
export AWS_DEFAULT_REGION=$REGION
echo "ok"

# parameters
CF_STACK_NAME="qsf-${STAGE_NAME}"
KEY_NAME="${CF_STACK_NAME}-${REGION}"

# check cloudformation stack exist
echo "Check if CloudFormation stack exists..."
CF_STACK_ID=$(aws cloudformation describe-stacks --stack-name ${CF_STACK_NAME} --query 'Stacks[0].StackId')
if [ $? -eq 0 ]; then
  echo "ERROR: CloudFormation stack already exist ${CF_STACK_ID}"
  exit
fi
echo "ok"
echo ""

# create EC2 key pair
echo "Check EC2 key pair: ${KEY_NAME}"
aws ec2 describe-key-pairs --key-name $KEY_NAME
if [ $? -ne 0 ]; then
  echo "Create a new key and save to $KEY_NAME.pem"
  aws ec2 create-key-pair --key-name $KEY_NAME --query 'KeyMaterial' --output text > $KEY_NAME.pem
fi
echo "ok"


# cloudformation deploy k8s cluster environment
echo ""
echo "Please confirm CloudFormation parameters below:"
echo "===================================================="
echo "  Stack Name: ${CF_STACK_NAME}"
echo "  CidrBlock : ${ClassAB}.0.0/16"
echo "  Key Name: ${KEY_NAME}"
echo "  EC2 basic Image ID: ${CENTOS69_IMAGE_ID}"
echo "===================================================="
echo ""

confirm=""
while [[ ! "$confirm" =~ ^[Y|y|N|n] ]]
do
  read -p "Do you want to continue? [y/n] " confirm
  if [[ "$confirm" =~ ^[N|n] ]]; then
    echo "Exit installation."
    exit
  fi
done

aws cloudformation deploy --template-file vpc-2azs.yaml --stack-name $CF_STACK_NAME --parameter-overrides ClassAB=${ClassAB} Env=$CF_STACK_NAME KeyName=$KEY_NAME ImageId=$CENTOS69_IMAGE_ID InstanceType=$INSTANCE_TYPE --capabilities CAPABILITY_IAM

# deploy failed, delete EC2 key
if [ $? -ne 0 ]; then
  echo "Cloudformation deploy failed."
  echo "Delete EC2 key pair..."
  rm -rf "$KEY_NAME.pem"
  aws ec2 delete-key-pair --key-name $KEY_NAME
  echo "delete ok"
  echo "ERROR: Cloudformation deploy failed! Please use the AWS CloudFormation console to view the status of your stack."
  exit
fi

# scp webInit to WebAmiEC2
echo "Scp webInit folder to WebAmiEC2..."
WebAmiEC2EIP=$(aws cloudformation list-exports --output text --query 'Exports[?Name==`'${CF_STACK_NAME}-WebAmiEC2EIP'`].Value')
echo "WebAmiEC2EIP = ${WebAmiEC2EIP}"
chmod 400 "$KEY_NAME.pem"
scp -i "$KEY_NAME.pem" "$KEY_NAME.pem" root@$WebAmiEC2EIP:/root # for login node
scp -r -i "$KEY_NAME.pem" webInit root@$WebAmiEC2EIP:/root
echo "ok"

WebLoadBalancerUrl=$(aws cloudformation list-exports --output text --query 'Exports[?Name==`'${CF_STACK_NAME}-WebLoadBalancerUrl'`].Value')
echo "Web ALB Url Link : ${WebLoadBalancerUrl}"
ApLoadBalancerUrl=$(aws cloudformation list-exports --output text --query 'Exports[?Name==`'${CF_STACK_NAME}-ApLoadBalancerUrl'`].Value')
echo "Ap ALB Url Link : ${ApLoadBalancerUrl}"
echo ""
LogsEC2EIP=$(aws cloudformation list-exports --output text --query 'Exports[?Name==`'${CF_STACK_NAME}-LogsEC2EIP'`].Value')
echo "Web Logs EIP : ${LogsEC2EIP}"
WebAmiEC2EIP=$(aws cloudformation list-exports --output text --query 'Exports[?Name==`'${CF_STACK_NAME}-WebAmiEC2EIP'`].Value')
echo "Web Ami EIP : ${WebAmiEC2EIP}"
echo ""
echo "all instance type : ${INSTANCE_TYPE}"

echo ""
echo "Finish!"
